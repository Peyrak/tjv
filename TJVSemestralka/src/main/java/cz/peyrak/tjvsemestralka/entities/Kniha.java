/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.entities;

import cz.peyrak.tjvsemestralkadto.KnihaDTO;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ondrej
 */
@Entity
@XmlRootElement
public class Kniha implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String name;
    
    @ManyToOne
    private Autor autor;
    
    @ManyToOne
    private Ctenar ctenar;

    public Kniha() {}
    
    public Kniha(KnihaDTO obj){
        this.id=obj.getId();
        this.name=obj.getName();
    }
    
    public KnihaDTO createDTO(){
        KnihaDTO res = new KnihaDTO();
        res.setId(this.id);
        res.setName(this.name);
        return res;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Ctenar getCtenar() {
        return ctenar;
    }

    public void setCtenar(Ctenar ctenar) {
        this.ctenar = ctenar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kniha)) {
            return false;
        }
        Kniha other = (Kniha) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.semestralkatjv.entities.Kniha[ id=" + id + " ]";
    }
    
}

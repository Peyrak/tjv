/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.controller;

import cz.peyrak.tjvsemestralka.controller.exceptions.NonexistentEntityException;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import cz.peyrak.tjvsemestralka.entities.Ctenar;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cz.peyrak.tjvsemestralka.entities.Kniha;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author ondrej
 */
@Stateless
public class CtenarJpaController {

    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ctenar ctenar) throws RollbackFailureException, Exception {
        if (ctenar.getKnihy() == null) {
            ctenar.setKnihy(new ArrayList<Kniha>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Kniha> attachedKnihy = new ArrayList<Kniha>();
            for (Kniha knihyKnihaToAttach : ctenar.getKnihy()) {
                knihyKnihaToAttach = em.getReference(knihyKnihaToAttach.getClass(), knihyKnihaToAttach.getId());
                attachedKnihy.add(knihyKnihaToAttach);
            }
            ctenar.setKnihy(attachedKnihy);
            em.persist(ctenar);
            for (Kniha knihyKniha : ctenar.getKnihy()) {
                Ctenar oldCtenarOfKnihyKniha = knihyKniha.getCtenar();
                knihyKniha.setCtenar(ctenar);
                knihyKniha = em.merge(knihyKniha);
                if (oldCtenarOfKnihyKniha != null) {
                    oldCtenarOfKnihyKniha.getKnihy().remove(knihyKniha);
                    oldCtenarOfKnihyKniha = em.merge(oldCtenarOfKnihyKniha);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ctenar ctenar) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Ctenar persistentCtenar = em.find(Ctenar.class, ctenar.getId());
            List<Kniha> knihyOld = persistentCtenar.getKnihy();
            List<Kniha> knihyNew = ctenar.getKnihy();
            List<Kniha> attachedKnihyNew = new ArrayList<Kniha>();
            for (Kniha knihyNewKnihaToAttach : knihyNew) {
                knihyNewKnihaToAttach = em.getReference(knihyNewKnihaToAttach.getClass(), knihyNewKnihaToAttach.getId());
                attachedKnihyNew.add(knihyNewKnihaToAttach);
            }
            knihyNew = attachedKnihyNew;
            ctenar.setKnihy(knihyNew);
            ctenar = em.merge(ctenar);
            for (Kniha knihyOldKniha : knihyOld) {
                if (!knihyNew.contains(knihyOldKniha)) {
                    knihyOldKniha.setCtenar(null);
                    knihyOldKniha = em.merge(knihyOldKniha);
                }
            }
            for (Kniha knihyNewKniha : knihyNew) {
                if (!knihyOld.contains(knihyNewKniha)) {
                    Ctenar oldCtenarOfKnihyNewKniha = knihyNewKniha.getCtenar();
                    knihyNewKniha.setCtenar(ctenar);
                    knihyNewKniha = em.merge(knihyNewKniha);
                    if (oldCtenarOfKnihyNewKniha != null && !oldCtenarOfKnihyNewKniha.equals(ctenar)) {
                        oldCtenarOfKnihyNewKniha.getKnihy().remove(knihyNewKniha);
                        oldCtenarOfKnihyNewKniha = em.merge(oldCtenarOfKnihyNewKniha);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ctenar.getId();
                if (findCtenar(id) == null) {
                    throw new NonexistentEntityException("The ctenar with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Ctenar ctenar;
            try {
                ctenar = em.getReference(Ctenar.class, id);
                ctenar.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ctenar with id " + id + " no longer exists.", enfe);
            }
            List<Kniha> knihy = ctenar.getKnihy();
            for (Kniha knihyKniha : knihy) {
                knihyKniha.setCtenar(null);
                knihyKniha = em.merge(knihyKniha);
            }
            em.remove(ctenar);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ctenar> findCtenarEntities() {
        return findCtenarEntities(true, -1, -1);
    }

    public List<Ctenar> findCtenarEntities(int maxResults, int firstResult) {
        return findCtenarEntities(false, maxResults, firstResult);
    }

    private List<Ctenar> findCtenarEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ctenar.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ctenar findCtenar(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ctenar.class, id);
        } finally {
            em.close();
        }
    }

    public int getCtenarCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ctenar> rt = cq.from(Ctenar.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

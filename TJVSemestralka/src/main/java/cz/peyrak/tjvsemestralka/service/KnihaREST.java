/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.service;

import cz.peyrak.tjvsemestralka.controller.KnihaJpaController;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import cz.peyrak.tjvsemestralka.entities.Kniha;
import cz.peyrak.tjvsemestralkadto.KnihaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ondrej
 */
@Stateless
@Path("kniha")
public class KnihaREST {

    KnihaJpaController cont;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(KnihaDTO kniha) throws Exception {
        cont.create(new Kniha(kniha));
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, KnihaDTO kniha) throws RollbackFailureException, Exception {
        cont.edit(new Kniha(kniha));
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) throws RollbackFailureException, Exception {
        cont.destroy(id);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Kniha find(@PathParam("id") Long id) {
        return cont.findKniha(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<KnihaDTO> findAll() {
        List<Kniha> temp = cont.findKnihaEntities();
        List<KnihaDTO> res = new ArrayList<KnihaDTO>();
        for (int i=0; i<temp.size(); i++){
            res.add(temp.get(i).createDTO());
        }
        return res;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Kniha> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return cont.findKnihaEntities(from, to);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(cont.getKnihaCount());
    }
    
}

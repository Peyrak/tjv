/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.service;

import cz.peyrak.tjvsemestralka.controller.CtenarJpaController;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import cz.peyrak.tjvsemestralka.entities.Ctenar;
import cz.peyrak.tjvsemestralkadto.CtenarDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ondrej
 */
@Stateless
@Path("ctenar")
public class CtenarREST {

    @EJB
    private CtenarJpaController cont;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(CtenarDTO ctenar) throws Exception {
        cont.create(new Ctenar(ctenar));
        
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, CtenarDTO ctenar) throws RollbackFailureException, Exception {
        cont.edit(new Ctenar(ctenar));
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) throws RollbackFailureException, Exception {
        cont.destroy(id);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CtenarDTO find(@PathParam("id") Long id) {
        return cont.findCtenar(id).createDTO();
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CtenarDTO> findAll() {
        List<Ctenar> temp = cont.findCtenarEntities();
        List<CtenarDTO> res = new ArrayList<CtenarDTO>();
        for (int i=0; i<temp.size(); i++){
            res.add(temp.get(i).createDTO());
        }
        return res;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ctenar> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return cont.findCtenarEntities(from, to);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(cont.getCtenarCount());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.service;

import cz.peyrak.tjvsemestralka.controller.AutorJpaController;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import cz.peyrak.tjvsemestralka.entities.Autor;
import cz.peyrak.tjvsemestralkadto.AutorDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ondrej
 */
@Stateless
@Path("autor")
public class AutorREST{
    @EJB
    private AutorJpaController cont;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(AutorDTO autor) throws Exception {
        cont.create(new Autor(autor));
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, AutorDTO autor) throws RollbackFailureException, Exception {
        cont.edit(new Autor(autor));
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) throws RollbackFailureException, Exception {
        cont.destroy(id);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AutorDTO find(@PathParam("id") Long id) {
        return cont.findAutor(id).createDTO();
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<AutorDTO> findAll() {
        List<AutorDTO> res = new ArrayList<>();
        res.add(new Autor(1, "Kokot").createDTO());
        return res;
        //List<Autor> temp = cont.findAutorEntities();
        //List<AutorDTO> res = new ArrayList<AutorDTO>();
        //for (int i=0; i<temp.size(); i++){
        //    res.add(temp.get(i).createDTO());
        //}
        //return res;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Autor> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return cont.findAutorEntities(from, to);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(cont.getAutorCount());
    }
    
}

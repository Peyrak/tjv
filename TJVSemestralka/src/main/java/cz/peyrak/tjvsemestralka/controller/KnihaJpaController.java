/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.controller;

import cz.peyrak.tjvsemestralka.controller.exceptions.NonexistentEntityException;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cz.peyrak.tjvsemestralka.entities.Autor;
import cz.peyrak.tjvsemestralka.entities.Ctenar;
import cz.peyrak.tjvsemestralka.entities.Kniha;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author ondrej
 */
@Stateless
public class KnihaJpaController{

    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Kniha kniha) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Autor autor = kniha.getAutor();
            if (autor != null) {
                autor = em.getReference(autor.getClass(), autor.getId());
                kniha.setAutor(autor);
            }
            Ctenar ctenar = kniha.getCtenar();
            if (ctenar != null) {
                ctenar = em.getReference(ctenar.getClass(), ctenar.getId());
                kniha.setCtenar(ctenar);
            }
            em.persist(kniha);
            if (autor != null) {
                autor.getKnihy().add(kniha);
                autor = em.merge(autor);
            }
            if (ctenar != null) {
                ctenar.getKnihy().add(kniha);
                ctenar = em.merge(ctenar);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Kniha kniha) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Kniha persistentKniha = em.find(Kniha.class, kniha.getId());
            Autor autorOld = persistentKniha.getAutor();
            Autor autorNew = kniha.getAutor();
            Ctenar ctenarOld = persistentKniha.getCtenar();
            Ctenar ctenarNew = kniha.getCtenar();
            if (autorNew != null) {
                autorNew = em.getReference(autorNew.getClass(), autorNew.getId());
                kniha.setAutor(autorNew);
            }
            if (ctenarNew != null) {
                ctenarNew = em.getReference(ctenarNew.getClass(), ctenarNew.getId());
                kniha.setCtenar(ctenarNew);
            }
            kniha = em.merge(kniha);
            if (autorOld != null && !autorOld.equals(autorNew)) {
                autorOld.getKnihy().remove(kniha);
                autorOld = em.merge(autorOld);
            }
            if (autorNew != null && !autorNew.equals(autorOld)) {
                autorNew.getKnihy().add(kniha);
                autorNew = em.merge(autorNew);
            }
            if (ctenarOld != null && !ctenarOld.equals(ctenarNew)) {
                ctenarOld.getKnihy().remove(kniha);
                ctenarOld = em.merge(ctenarOld);
            }
            if (ctenarNew != null && !ctenarNew.equals(ctenarOld)) {
                ctenarNew.getKnihy().add(kniha);
                ctenarNew = em.merge(ctenarNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = kniha.getId();
                if (findKniha(id) == null) {
                    throw new NonexistentEntityException("The kniha with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Kniha kniha;
            try {
                kniha = em.getReference(Kniha.class, id);
                kniha.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The kniha with id " + id + " no longer exists.", enfe);
            }
            Autor autor = kniha.getAutor();
            if (autor != null) {
                autor.getKnihy().remove(kniha);
                autor = em.merge(autor);
            }
            Ctenar ctenar = kniha.getCtenar();
            if (ctenar != null) {
                ctenar.getKnihy().remove(kniha);
                ctenar = em.merge(ctenar);
            }
            em.remove(kniha);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Kniha> findKnihaEntities() {
        return findKnihaEntities(true, -1, -1);
    }

    public List<Kniha> findKnihaEntities(int maxResults, int firstResult) {
        return findKnihaEntities(false, maxResults, firstResult);
    }

    private List<Kniha> findKnihaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Kniha.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Kniha findKniha(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Kniha.class, id);
        } finally {
            em.close();
        }
    }

    public int getKnihaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Kniha> rt = cq.from(Kniha.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.peyrak.tjvsemestralka.controller;

import cz.peyrak.tjvsemestralka.controller.exceptions.NonexistentEntityException;
import cz.peyrak.tjvsemestralka.controller.exceptions.RollbackFailureException;
import cz.peyrak.tjvsemestralka.entities.Autor;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cz.peyrak.tjvsemestralka.entities.Kniha;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author ondrej
 */
@Stateless
public class AutorJpaController {

    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Autor autor) throws RollbackFailureException, Exception {
        if (autor.getKnihy() == null) {
            autor.setKnihy(new ArrayList<Kniha>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Kniha> attachedKnihy = new ArrayList<Kniha>();
            for (Kniha knihyKnihaToAttach : autor.getKnihy()) {
                knihyKnihaToAttach = em.getReference(knihyKnihaToAttach.getClass(), knihyKnihaToAttach.getId());
                attachedKnihy.add(knihyKnihaToAttach);
            }
            autor.setKnihy(attachedKnihy);
            em.persist(autor);
            for (Kniha knihyKniha : autor.getKnihy()) {
                Autor oldAutorOfKnihyKniha = knihyKniha.getAutor();
                knihyKniha.setAutor(autor);
                knihyKniha = em.merge(knihyKniha);
                if (oldAutorOfKnihyKniha != null) {
                    oldAutorOfKnihyKniha.getKnihy().remove(knihyKniha);
                    oldAutorOfKnihyKniha = em.merge(oldAutorOfKnihyKniha);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Autor autor) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Autor persistentAutor = em.find(Autor.class, autor.getId());
            List<Kniha> knihyOld = persistentAutor.getKnihy();
            List<Kniha> knihyNew = autor.getKnihy();
            List<Kniha> attachedKnihyNew = new ArrayList<Kniha>();
            for (Kniha knihyNewKnihaToAttach : knihyNew) {
                knihyNewKnihaToAttach = em.getReference(knihyNewKnihaToAttach.getClass(), knihyNewKnihaToAttach.getId());
                attachedKnihyNew.add(knihyNewKnihaToAttach);
            }
            knihyNew = attachedKnihyNew;
            autor.setKnihy(knihyNew);
            autor = em.merge(autor);
            for (Kniha knihyOldKniha : knihyOld) {
                if (!knihyNew.contains(knihyOldKniha)) {
                    knihyOldKniha.setAutor(null);
                    knihyOldKniha = em.merge(knihyOldKniha);
                }
            }
            for (Kniha knihyNewKniha : knihyNew) {
                if (!knihyOld.contains(knihyNewKniha)) {
                    Autor oldAutorOfKnihyNewKniha = knihyNewKniha.getAutor();
                    knihyNewKniha.setAutor(autor);
                    knihyNewKniha = em.merge(knihyNewKniha);
                    if (oldAutorOfKnihyNewKniha != null && !oldAutorOfKnihyNewKniha.equals(autor)) {
                        oldAutorOfKnihyNewKniha.getKnihy().remove(knihyNewKniha);
                        oldAutorOfKnihyNewKniha = em.merge(oldAutorOfKnihyNewKniha);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = autor.getId();
                if (findAutor(id) == null) {
                    throw new NonexistentEntityException("The autor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Autor autor;
            try {
                autor = em.getReference(Autor.class, id);
                autor.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The autor with id " + id + " no longer exists.", enfe);
            }
            List<Kniha> knihy = autor.getKnihy();
            for (Kniha knihyKniha : knihy) {
                knihyKniha.setAutor(null);
                knihyKniha = em.merge(knihyKniha);
            }
            em.remove(autor);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Autor> findAutorEntities() {
        return findAutorEntities(true, -1, -1);
    }

    public List<Autor> findAutorEntities(int maxResults, int firstResult) {
        return findAutorEntities(false, maxResults, firstResult);
    }

    private List<Autor> findAutorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Autor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Autor findAutor(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Autor.class, id);
        } finally {
            em.close();
        }
    }

    public int getAutorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Autor> rt = cq.from(Autor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
